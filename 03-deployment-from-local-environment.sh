#!/bin/bash

HEROKU_APP_NAME=tech-u-bank-g10-backend

npm run build
# heroku login
heroku container:login
heroku container:push web -a $HEROKU_APP_NAME
heroku container:release web -a $HEROKU_APP_NAME
heroku logs -t -a $HEROKU_APP_NAME