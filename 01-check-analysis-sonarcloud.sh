#!/bin/bash

echo "BITBUCKET_REPO_OWNER: $BITBUCKET_REPO_OWNER"
echo "BITBUCKET_REPO_SLUG: $BITBUCKET_REPO_SLUG"
echo "BITBUCKET_REPO_UUID: $BITBUCKET_REPO_UUID"
echo "BITBUCKET_REPO_FULL_NAME: $BITBUCKET_REPO_FULL_NAME"
echo "BITBUCKET_BRANCH: $BITBUCKET_BRANCH"
echo "BITBUCKET_COMMIT: $BITBUCKET_COMMIT"
echo "BITBUCKET_WORKSPACE: $BITBUCKET_WORKSPACE"
echo "BITBUCKET_BUILD_NUMBER: $BITBUCKET_BUILD_NUMBER"
echo "BITBUCKET_GIT_HTTP_ORIGIN: $BITBUCKET_GIT_HTTP_ORIGIN"

STATUS_ANALYZE=$(curl -s "https://sonarcloud.io/api/qualitygates/project_status?projectKey=juanquedena_techu-backend&branch=$BITBUCKET_BRANCH" | \
    python2 -c "import sys, json; print json.load(sys.stdin)['projectStatus']['status']")

echo "STATUS_ANALYZE: $STATUS_ANALYZE"

if [[ $STATUS_ANALYZE == 'ERROR' ]]; then
  echo "failed"
  exit 1
else
  echo "success"
  exit 0
fi