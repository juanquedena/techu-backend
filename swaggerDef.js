const dotenv = require('dotenv');
dotenv.config();

const { PORT, API_URL } = process.env;

module.exports = {
  info: {
    version: '1.0.0',
    title: 'API TechU - Particioner 2020 - 1',
    description: 'API Information',
  },
  host: `localhost:${PORT}`,
  basePath: API_URL,
  schemes: ['http'],
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'Authorization',
      scheme: 'bearer',
      in: 'header',
    },
  },
  tags: [
    { name: 'Authentication' }
  ],
  apis: [`${__dirname}/src/**/*.ts`, `${__dirname}/src/**/*.js`],
};