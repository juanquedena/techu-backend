declare namespace Express {

  export interface Request {
     userAuthenticate?: object;
     payloadVerified?: object;
  }
}

declare module "*.json" {
  const value: any;
  export default value;
}
