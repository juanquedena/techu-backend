#!/bin/bash

# BITBUCKET_REPO_OWNER=juanquedena
# BITBUCKET_REPO_SLUG=techu-backend
# BITBUCKET_REPO_UUID={430ac34e-0059-4d87-b281-2c968ff643d9}
# BITBUCKET_REPO_FULL_NAME=juanquedena/techu-backend
# BITBUCKET_BRANCH=feature/02-configure-pipeline
# BITBUCKET_COMMIT=1a0f2b9b18df
# BITBUCKET_WORKSPACE=juanquedena
# BITBUCKET_BUILD_NUMBER=41	
# BITBUCKET_GIT_HTTP_ORIGIN=http://bitbucket.org/juanquedena/techu-backend

workspace=$BITBUCKET_REPO_OWNER
repository=$BITBUCKET_REPO_SLUG
buildNumber=$BITBUCKET_BUILD_NUMBER
commitHash=$BITBUCKET_COMMIT
gitBranch=$BITBUCKET_BRANCH
gitOrigin=$BITBUCKET_GIT_HTTP_ORIGIN
buildUrl="https://bitbucket.org/$workspace/$repository/addon/pipelines/home#!/results/$buildNumber"

# Get Git commit info
if [ $buildNumber -eq 1 ]
then
  echo "Getting commits for $commitHash"
  git log --pretty=oneline $commitHash >> changes.txt
else
  previousbuildNumber=$(expr $buildNumber - 1)
  baseURL="https://api.bitbucket.org/2.0/repositories/$workspace/$repository/pipelines"

  prevCommitHashURL="$baseURL/$previousbuildNumber"
  prevFullHash=$(curl -s -X GET "$prevCommitHashURL" | \
    python2 -c "import sys, json; print json.load(sys.stdin)['target']['commit']['hash']")

  prevHash="${prevFullHash%\"}"
  prevHash="${prevFullHash#\"}"
  commitHash="${commitHash%\"}"
  commitHash="${commitHash#\"}"

  prevHash=$(echo $prevHash | cut -b 1-7)
  commitHash=$(echo $commitHash | cut -b 1-7)

  echo "Comparing between $prevHash and $commitHash"
  git log --pretty=oneline "$prevHash"..$commitHash >> changes.txt
fi

if [[ $BITBUCKET_BRANCH == "developer" || $BITBUCKET_BRANCH == "feature"* ]]
then
  TYPE_TAG="snapshot"
elif [ $BITBUCKET_BRANCH == "master" ]
then
  TYPE_TAG="release"
else
  TYPE_TAG="staging"
fi

git add changes.txt
git commit -m "Updating changes.txt with the change list for build."
git tag -am "Tagging for ${TYPE_TAG} ${BITBUCKET_BUILD_NUMBER}" "${TYPE_TAG}-${BITBUCKET_BUILD_NUMBER}"
git push origin "${TYPE_TAG}-${BITBUCKET_BUILD_NUMBER}"
echo "Branch: $BITBUCKET_BRANCH, Tagging for ${TYPE_TAG} ${BITBUCKET_BUILD_NUMBER}"