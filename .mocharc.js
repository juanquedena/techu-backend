module.exports = {
  reporter: 'node_modules/mochawesome',
  'reporter-option': [
      'overwrite=true',
      'reportTitle=techu-backend',
      'charts=true',
      'showHooks=always',
  ],
};