type Transform = (s: string) => string;

export class FunctionUtil {

  public static snakeToCamel: Transform = (s: string) => s.replace(/(\_\w)/g, (m: string) => m[1].toUpperCase());

  public static camelToSnake: Transform = (s: string) => s.replace(/([A-Z]\w)/g, (m: string) => '_' + m.toLowerCase());

  private static transform(o: any, method: Transform): any {
    const oNew: any = {};
    for (const key in o) {
      if (o.hasOwnProperty(key)) {
        const e = o[key];
        if (e instanceof Array) {
          const a = [];
          for (const row of e) {
            a.push(this.transform(row, method));
          }
          oNew[method(key)] = a;
        } else if (e instanceof Object) {
          oNew[method(key)] = this.transform(e, method);
        } else {
          oNew[method(key)] = e;
        }
      }
    }
    return oNew;
  }

  public static transPropsSnakeToCamel(o: any): any {
    return this.transform(o, this.snakeToCamel);
  }

  public static transPropsCamelToSnake(o: any): any {
    return this.transform(o, this.camelToSnake);
  }
}
