import * as express from 'express';
import authenticateMiddleware from './middlewares/authenticateMiddleware';
import verifySignatureMiddleware from './middlewares/verifySignatureMiddleware';
import SecureController from './controllers/SecureController';
import UserController from './controllers/UserController';
import AccountController from './controllers/AccountController';
import App from './App';
import TransactionController from './controllers/TransactionController';

const { PORT, API_URL } = process.env;

const initCallback = (app: express.Application) => {
  console.log('Init Callback');
};

export const appSecure = new App(
  API_URL,
  PORT,
  initCallback,
  [
    new SecureController(),
    new UserController(),
    new AccountController(),
    new TransactionController(),
  ],
  [
    authenticateMiddleware,
    verifySignatureMiddleware,
  ]);
