import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { Message } from '../models/models';

const { MONGO_URL, MONGO_API_KEY } = process.env;

const transform = (data: any) => {
  let element: any;
  if (data instanceof Array) {
    element = [];
    for (const row of data) {
      element.push(transform(row));
    }
  } else if (data instanceof Object) {
    element = {};
    for (const key in data) {
      if (data[key].$numberInt !== undefined) {
        element[key] = parseInt(data[key].$numberInt, 10);
      } else if (data[key].$numberDouble !== undefined) {
        element[key] = parseFloat(data[key].$numberDouble);
      } else {
        element[key] = transform(data[key]);
      }
    }
  } else {
    element = data;
  }
  return element;
};

const config: AxiosRequestConfig = {
  withCredentials: false,
  timeout: 10000,
  baseURL: MONGO_URL,
  headers: {
    common: {
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      'Content-Type': 'text/plain',
      Pragma: 'no-cache',
      Apikey: MONGO_API_KEY,
    },
  },
};

class BaseRepository {

  private axiosClient: AxiosInstance;
  private collection: string;

  constructor(collection: string) {
    this.axiosClient = axios.create(config);
    this.collection = collection;
  }

  async invokeAutoincrement(collection?: string) {
    const oid = { _id: collection || this.collection };
    const updData = { $inc: { sequence_value: 1 } };
    return this.invokePost('increment', updData, oid, 'counters')
      .then((response: any) => response.sequence_value);
  }

  async invokePost(operation: string, data: any, oid?: any, collection?: string) {
    const message: Message = {
      header: {
        applicationCode: '001-1',
        channelCode: '001-1',
      },
      payload: {
        oid,
        operation,
        collection: collection || this.collection,
        item: data,
      },
    };
    const payload = Buffer.from(JSON.stringify(message)).toString('base64');
    return this.axiosClient.post('', payload, config)
      .then((response: AxiosResponse) => {
        return transform(response.data);
      });
  }

  async invokeGet(find: object, fields?: object, sort?: object, pageSize: number = 10, page: number = 1) {
    const aggregation = [
      { $match: find },
      { $project: fields || { _id: 0 } },
      { $sort: sort || { _id: 1 } },
      {
        $facet: {
          metadata: [
            { $count: 'total' },
            { $addFields: { page } },
          ],
          data: [
            { $skip: (page - 1) * pageSize },
            { $limit: pageSize },
          ],
        },
      },
    ];
    return this.invokePost('get', aggregation)
      .then((response: any) => {
        const { metadata, data } = response[0];
        const pagination = {
          page: 0,
          totalPages: 0,
        };
        if (metadata.length > 0) {
          pagination.page = metadata[0].page;
          pagination.totalPages = Math.ceil(metadata[0].total / pageSize);
        }
        return { data, pagination };
      });
  }
}

export default BaseRepository;
