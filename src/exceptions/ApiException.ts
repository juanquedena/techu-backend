export type CodeError =
  'S01' | 'S02' | 'S03' |
  'F01' | 'F02' | 'F03' | 'F04' |
  'V00' | 'V01' |
  'Z97' | 'Z98' | 'Z99';

const errors:any = {
  S01: 'Usuario no autorizado',
  S02: 'Acceso no autorizado',
  S03: 'No se encontraron resultados',
  F01: 'Usuario no existe',
  F02: 'Contraseña incorrecta',
  F03: 'Cuenta no existe',
  F04: 'Transacción no existe',
  V00: 'Mensaje invalido',
  V01: 'Errores de validación',
};

export class ApiException extends Error {

  status: number;
  messages: any[] = [];
  code: string;

  constructor(status: number, code: CodeError) {
    super();
    this.message = errors[code];
    this.status = status;
    this.code = code;
  }

  addAllMessages(messages: any[]) {
    this.messages = [...messages];
  }

  getError() {
    const error:any = { code: this.code, status: this.status };
    if (this.messages.length === 0) {
      error.message = this.message;
    } else {
      error.messages = this.messages;
    }
    return error;
  }
}
