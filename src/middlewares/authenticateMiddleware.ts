import * as express from 'express';
import { ApiException } from '../exceptions/ApiException';
import AuthenticationHelper from '../helper/authenticationHelper';

const { API_URL } = process.env;

const WHITE_LIST = [`${API_URL}/secure/signIn`, `${API_URL}/unsecure/users`, '/swagger.json'];

export default function authenticateMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
  console.log(`${request.method} ${request.path}`);
  // console.log('Headers: ' + JSON.stringify(request.headers));
  if (WHITE_LIST.indexOf(request.path) > -1 || request.path.indexOf(`${API_URL}/api-docs/`) > -1) {
    next();
  } else {
    const authHeader = request.headers.authorization;
    if (authHeader) {
      const token = authHeader.split(' ')[1];
      AuthenticationHelper.verifyAccessToken(token, (err, user) => {
        if (err) {
          throw new ApiException(403, 'S02');
        }
        request.userAuthenticate = user;
      });
      next();
    } else {
      throw new ApiException(401, 'S01');
    }
  }
}
