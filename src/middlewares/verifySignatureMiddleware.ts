import * as express from 'express';
import { ApiException } from '../exceptions/ApiException';
import AuthenticationHelper from '../helper/authenticationHelper';

const { NODE_ENV } = process.env;

export default function verifySignatureMiddleware(
    request: express.Request, response: express.Response, next: express.NextFunction) {
  if (NODE_ENV !== 'development' && !(request.method === 'GET' || request.method === 'DELETE') && request.body) {
    const { payloadSignature } = request.body;
    AuthenticationHelper.verifySignature(payloadSignature, request.headers, (err, payloadVerified) => {
      if (err) {
        throw new ApiException(400, 'V00');
      }
      request.payloadVerified = payloadSignature;
      request.body = payloadVerified;
    });
  }
  next();
}
