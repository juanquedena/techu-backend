import * as express from 'express';
import * as expressValidator from 'express-validator';
import { ApiException } from '../exceptions/ApiException';

const { validationResult } = expressValidator;

export default function validationMiddleware(next: (request: express.Request, response: express.Response) => void) {
  return (request: express.Request, response: express.Response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      const validationError = new ApiException(400, 'V01');
      validationError.addAllMessages(errors.array());
      throw validationError;
    } else {
      next(request, response);
    }
  };
}
