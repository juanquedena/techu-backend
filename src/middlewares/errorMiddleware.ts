import * as express from 'express';
import { ApiException } from '../exceptions/ApiException';

export default function errorMiddleware(err: Error, request: express.Request, response: express.Response, next: any) {
  if (response.headersSent) {
    return next(err);
  }

  if (err instanceof ApiException) {
    response.status(err.status).json(err.getError());
  } else {
    response.status(500).json({ detail: err, code: 'Z99', status: 500, message: err.message });
  }
}
