import * as express from 'express';
import BaseController from './BaseControllerInterface';
import TransactionSchema from '../models/transactionSchema';
import BaseSchema from '../models/baseSchema';
import validationMiddleware from '../middlewares/validationMiddleware';
import TransactionService from '../services/TransactionService';
import { responseOkHelper, responseErrorHelper } from '../helper/responseHelper';
import { queryTransform, getNumberParam } from '../helper/requestHelper';

/**
 * @apiDefine TransactionResponse
 * @apiSuccess {Number} data.transactionId Identificador de la transacción
 * @apiSuccess {String} data.date Fecha del movimiento
 * @apiSuccess {Number} data.amount Importe del movimiento
 * @apiSuccess {String} data.description Descripcion del movimiento
 * @apiSuccess {String="SHOULD","TO HAVE"} data.type Tipo de movimiento
 * @apiSuccess {Number} data.userId Identificador del usuario de origen
 * @apiSuccess {Number} data.accountId Identificador de la cuenta de origen
 * @apiSuccess {Number} data.userReceiveId Identificador del usuario de destino
 * @apiSuccess {Number} data.accountReceiveId Identificador de la cuenta de destino
 */

/**
 * @apiDefine TransactionParams
 * @apiParam {String} data.date Fecha del movimiento
 * @apiParam {Number} data.amount Importe del movimiento
 * @apiParam {String} data.description Descripcion del movimiento
 * @apiParam {String="SHOULD","TO HAVE"} data.type Tipo de movimiento
 * @apiParam {Number} data.userId Identificador del usuario de origen
 * @apiParam {Number} data.accountId Identificador de la cuenta de origen
 * @apiParam {Number} data.userReceiveId Identificador del usuario de destino
 * @apiParam {Number} data.accountReceiveId Identificador de la cuenta de destino
 */

class TransactionController implements BaseController {
  public path = '/transactions';
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path, BaseSchema.querySchema, validationMiddleware(this.query));
    this.router.get(`${this.path}/:transactionId`, TransactionSchema.transactionIdSchema, validationMiddleware(this.get));
    this.router.post(`${this.path}`, TransactionSchema.transactionPostSchema, validationMiddleware(this.save));
    this.router.patch(
      `${this.path}/:transactionId`,
      TransactionSchema.transactionPatchSchema,
      validationMiddleware(this.replace));
  }

/**
 * @api {post} /transtacions 01 - Crear transacción
 * @apiVersion 1.0.0
 * @apiName Post
 * @apiGroup Transaction
 * @apiPermission authenticate
 *
 * @apiDescription Crear una transacción
 *
 * @apiUse HeadersParams
 *
 * @apiUse TransactionParams
 *
 * @apiSuccess {Object} data Datos de la transacción
 * @apiUse TransactionResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  save = (request: express.Request, response: express.Response) => {
    TransactionService.save(request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {patch} /transtacions/:transactionId 02 - Actualizar la transacción
 * @apiVersion 1.0.0
 * @apiName Patch
 * @apiGroup Transaction
 * @apiPermission authenticate
 *
 * @apiDescription Actualizar una transacción
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} transactionId Identificador de la transacción
 * @apiUse TransactionParams
 *
 * @apiSuccess {Object} data Datos de la transacción
 * @apiUse TransactionResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  replace = (request: express.Request, response: express.Response) => {
    TransactionService.replace(getNumberParam(request, 'transactionId'), request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {get} /transactions/:transactionId 03 - Obtener transacción
 * @apiVersion 1.0.0
 * @apiName Get
 * @apiGroup Transaction
 * @apiPermission authenticate
 *
 * @apiDescription Obtener transacción
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} transactionId Identificador de la transacción
 *
 * @apiSuccess {Object} data Datos de la transacción
 * @apiUse TransactionResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (204 Errores funcionales) F04 Transacción no existe
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  get = (request: express.Request, response: express.Response) => {
    TransactionService.get(getNumberParam(request, 'transactionId'))
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {get} /transactions?query=:query&sort=:sort&page=:page&pageSize=:pageSize 04 - Listar transacciones
 * @apiVersion 1.0.0
 * @apiName GetAll
 * @apiGroup Transaction
 * @apiPermission authenticate
 *
 * @apiDescription Listar transacciones
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} query Consulta a realizar, ejemplo query={userId:12}
 * @apiParam {String} sort Campos por los que se ordena el resultado, ejemplo sort=firstName:1
 * @apiParam {Number} page Página a mostrar
 * @apiParam {Number} pageSize Número de filas por página
 *
 * @apiSuccess {Object[]} data Datos de la transacción
 * @apiUse TransactionResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (204 Errores funcionales) S03 No se encontraron resultados
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  query = (request: express.Request, response: express.Response) => {
    TransactionService.query(queryTransform(request))
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }
}

export default TransactionController;
