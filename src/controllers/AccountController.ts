import * as express from 'express';
import BaseController from './BaseControllerInterface';
import AccountSchema from '../models/accountSchema';
import BaseSchema from '../models/baseSchema';
import validationMiddleware from '../middlewares/validationMiddleware';
import AccountService from '../services/AccountService';
import { responseOkHelper, responseErrorHelper } from '../helper/responseHelper';
import { queryTransform, getNumberParam } from '../helper/requestHelper';

/**
 * @apiDefine AccountsResponse
 * @apiSuccess {Number} data.accountId Identificador de la cuenta
 * @apiSuccess {String} data.iban Código de la cuenta
 * @apiSuccess {String="PEN","USD","EUR"} data.currencyCode Código de la moneda
 * @apiSuccess {Number} data.balance Saldo de la cuenta
 * @apiSuccess {Object} data.card Datos de la tarjeta
 * @apiSuccess {String} data.card.number Número de la tarjeta
 * @apiSuccess {String="Visa","Master Card"} data.card.type Tipo de tarjeta
 */

/**
 * @apiDefine AccountsParams
 * @apiParam {String} iban Código de la cuenta
 * @apiParam {String="PEN","USD","EUR"} currencyCode Código de la moneda
 * @apiParam {Number} balance Saldo de la cuenta
 * @apiParam {Object} card Datos de la tarjeta
 * @apiParam {String} card.number Número de la tarjeta
 * @apiParam {String="Visa","Master Card"} card.type Tipo de tarjeta
 */
class AccountController implements BaseController {
  public path = '/users/:userId/accounts';
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get('/accounts', BaseSchema.querySchema, validationMiddleware(this.queryAccount));
    this.router.get(this.path, BaseSchema.querySchema, validationMiddleware(this.query));
    this.router.get(`${this.path}/:accountId`, AccountSchema.accountIdSchema, validationMiddleware(this.get));
    this.router.post(`${this.path}`, AccountSchema.accountPostSchema, validationMiddleware(this.save));
    this.router.patch(`${this.path}/:accountId`, AccountSchema.accountPatchSchema, validationMiddleware(this.replace));
  }

/**
 * @api {post} /users/:userId/accounts 01 - Crear Cuenta
 * @apiVersion 1.0.0
 * @apiName Post
 * @apiGroup User-Account
 * @apiPermission authenticate
 *
 * @apiDescription Crear una cuenta asociada a un usuario
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} userId Identificador de usuario
 *
 * @apiUse AccountsParams
 *
 * @apiUse AccountsResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  save = (request: express.Request, response: express.Response) => {
    AccountService.save(getNumberParam(request, 'userId'), request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {post} /users/:userId/accounts/:accountId 02 - Actualizar Cuenta
 * @apiVersion 1.0.0
 * @apiName Patch
 * @apiGroup User-Account
 * @apiPermission authenticate
 *
 * @apiDescription Actualiza una cuenta asociada a un usuario
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} userId Identificador de usuario
 * @apiParam {String} accountId Identificador de cuenta
 *
 * @apiUse AccountsParams
 *
 * @apiSuccess {Object} data Datos de la cuenta
 * @apiUse AccountsResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  replace = (request: express.Request, response: express.Response) => {
    AccountService.replace(
        getNumberParam(request, 'userId'),
        getNumberParam(request, 'accountId'),
        request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

  /**
   * @api {get} /users/:userId/accounts/:accountId 03 - Obtener Cuentas
   * @apiVersion 1.0.0
   * @apiName Get
   * @apiGroup User-Account
   * @apiPermission authenticate
   *
   * @apiDescription Obtener una cuenta asignada a un usuario
   *
   * @apiUse HeadersParams
   *
   * @apiParam {String} userId Identificador de usuario
   * @apiParam {String} accountId Identificador de cuenta
   *
   * @apiSuccess {Object} data Datos de la cuenta
   * @apiUse AccountsResponse
   *
   * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
   * @apiError (400 Errores funcionales) F03 Cuenta no existe
   * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
   */
  get = (request: express.Request, response: express.Response) => {
    AccountService.get(
        getNumberParam(request, 'userId'),
        getNumberParam(request, 'accountId'))
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {get} /users/:userId/accounts/:accountId 04 - Listar Cuentas
 * @apiVersion 1.0.0
 * @apiName GetAll
 * @apiGroup User-Account
 * @apiPermission authenticate
 *
 * @apiDescription Listar usuarios
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} userId Identificador de usuario
 * @apiParam {String} accountId Identificador de cuenta
 *
 * @apiSuccess {Object[]} data Datos de la cuenta
 * @apiUse AccountsResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (204 Errores funcionales) S03 No se encontraron resultados
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  query = (request: express.Request, response: express.Response) => {
    AccountService.query(getNumberParam(request, 'userId'), queryTransform(request))
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

  /**
   * @api {get} /accounts?iban=:iban 00 - Obtener Cuenta por IBAN
   * @apiVersion 1.0.0
   * @apiName Get
   * @apiGroup Account
   * @apiPermission authenticate
   *
   * @apiDescription Obtener una cuenta por IBAN
   *
   * @apiUse HeadersParams
   *
   * @apiParam {String} iban Código IBAN
   *
   * @apiSuccess {Object} data Datos de la cuenta
   * @apiUse AccountsResponse
   * @apiSuccess {Object} data.user Datos del usuario
   * @apiSuccess {Number} data.user.userId Identificador
   * @apiSuccess {String} data.user.firstName Nombres
   * @apiSuccess {String} data.user.lastName Apellidos
   *
   * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
   * @apiError (400 Errores funcionales) F03 Cuenta no existe
   * @apiError (204 Errores funcionales) S03 No se encontraron resultados
   * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
   */
  queryAccount = (request: express.Request, response: express.Response) => {
    const { iban } = request.query;
    AccountService.queryAccount(iban)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }
}

export default AccountController;
