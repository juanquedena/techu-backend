import * as express from 'express';

interface BaseController {
  intializeRoutes(): void;
  router: express.Router;
}

export default BaseController;
