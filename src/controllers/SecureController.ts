import * as express from 'express';
import BaseController from './BaseControllerInterface';
import AuthenticateSchema from '../models/authenticateSchema';
import SecureService from '../services/SecureService';
import UserService from '../services/UserService';
import validationMiddleware from '../middlewares/validationMiddleware';
import { responseOkHelper, responseErrorHelper } from '../helper/responseHelper';
import SecureSchema from '../models/secureSchema';

class SecureController implements BaseController {
  public path = '/secure';
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.post(this.path, SecureSchema.secureSchema, validationMiddleware(this.sendDocument));
    this.router.post(`${this.path}/signIn`, AuthenticateSchema, validationMiddleware(this.authenticate));
  }

  sendDocument = (request: express.Request, response: express.Response) => {
    const authToken = request.header('auth-token');
    response.json(SecureService.sendDocument(request.body, authToken));
  }

/**
 * @api {post} /secure/signIn 00 - Autentificación
 * @apiVersion 1.0.0
 * @apiName SignIn
 * @apiGroup User
 * @apiPermission none
 *
 * @apiDescription Servicio de autentificación de usuarios
 *
 * @apiHeader {String} signature-issuer Aplicación Cliente
 * @apiHeader {String} signature-subject Organización Cliente
 *
 * @apiParam {String} username Nombre de usuario
 * @apiParam {String} password Contraseña de usuario
 *
 * @apiSuccess {Object} data Datos del usuario
 * @apiSuccess {String} data.userId Identificador
 * @apiSuccess {String} data.firstName Nombres
 * @apiSuccess {String} data.lastName Apellidos
 * @apiSuccess {Object} authorization Datos de autorización
 * @apiSuccess {String} authorization.accessToken Token de acceso
 * @apiSuccess {String} authorization.refreshToken Token de refresco
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (400 Errores funcionales) F01 Usuario no existe
 * @apiError (400 Errores funcionales) F02 Contraseña incorrecta
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  authenticate = (request: express.Request, response: express.Response) => {
    const { username, password } = request.body;
    UserService.authenticate(username, password)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }
}

export default SecureController;
