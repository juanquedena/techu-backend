import * as express from 'express';
import BaseController from './BaseControllerInterface';
import UserSchema from '../models/userSchema';
import BaseSchema from '../models/baseSchema';
import validationMiddleware from '../middlewares/validationMiddleware';
import UserService from '../services/UserService';
import { responseOkHelper, responseErrorHelper } from '../helper/responseHelper';
import { queryTransform, getNumberParam } from '../helper/requestHelper';

class UserController implements BaseController {
  public path = '/users';
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path, BaseSchema.querySchema, validationMiddleware(this.query));
    this.router.get(`${this.path}/:userId`, UserSchema.userIdSchema, validationMiddleware(this.get));
    this.router.post(`${this.path}`, UserSchema.userPostSchema, validationMiddleware(this.save));
    this.router.post(`/unsecure/${this.path}`, UserSchema.userPostSchema, validationMiddleware(this.save));
    this.router.patch(`${this.path}/:userId`, UserSchema.userPatchSchema, validationMiddleware(this.replace));
  }

/**
 * @api {post} /users 01 - Crear Usuario
 * @apiVersion 1.0.0
 * @apiName Post
 * @apiGroup User
 * @apiPermission authenticate
 *
 * @apiDescription Crear un usuario
 *
 * @apiUse HeadersParams
 *
 * @apiUse UserParams
 *
 * @apiSuccess {Object} data Datos del usuario
 * @apiUse UserResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  save = (request: express.Request, response: express.Response) => {
    UserService.save(request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {patch} /users/:userId 02 - Actualizar Usuario
 * @apiVersion 1.0.0
 * @apiName Patch
 * @apiGroup User
 * @apiPermission authenticate
 *
 * @apiDescription Actualizar un usuario
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} userId Identificador de usuario
 * @apiUse UserParams
 *
 * @apiSuccess {Object} data Datos del usuario
 * @apiUse UserResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  replace = (request: express.Request, response: express.Response) => {
    UserService.replace(getNumberParam(request, 'userId'), request.body)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @apiDefine HeadersParams
 * @apiHeader {String} Authorization Token para acceder a los servicios
 * @apiHeader {String} signature-issuer Aplicación Cliente
 * @apiHeader {String} signature-subject Organización Cliente
 * @apiHeader {String} token-refresh Token de refresco
 */

/**
 * @apiDefine UserParams
 * @apiParam {String} firstName Nombres
 * @apiParam {String} lastName Apellidos
 * @apiParam {String} email Correo electrónico
 * @apiParam {String} password Contraseña
 */

/**
 * @apiDefine UserResponse
 * @apiSuccess {Number} data.userId Identificador
 * @apiSuccess {String} data.firstName Nombres
 * @apiSuccess {String} data.lastName Apellidos
 * @apiSuccess {String} data.email Correo electrónico
 */

/**
 * @apiDefine AccountResponse
 * @apiSuccess {Object[]} data.accounts Datos de las cuentas
 * @apiSuccess {Number} data.accounts.accountId Identificador de la cuenta
 * @apiSuccess {String} data.accounts.iban Código de la cuenta
 * @apiSuccess {String="PEN","USD","EUR"} data.accounts.currencyCode Código de la moneda
 * @apiSuccess {Number} data.accounts.balance Saldo de la cuenta
 * @apiSuccess {Object} data.accounts.card Datos de la tarjeta
 * @apiSuccess {String} data.accounts.card.number Número de la tarjeta
 * @apiSuccess {String="Visa","Master Card"} data.accounts.card.type Tipo de tarjeta
 */

/**
 * @api {get} /users/:userId?expands=:expands 03 - Obtener Usuario
 * @apiVersion 1.0.0
 * @apiName Get
 * @apiGroup User
 * @apiPermission authenticate
 *
 * @apiDescription Obtener un usuario
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} userId Identificador de usuario
 * @apiParam {String="accounts"} expands Mostrar el detalle de una subcolección
 *
 * @apiSuccess {Object} data Datos del usuario
 * @apiUse UserResponse
 * @apiUse AccountResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (400 Errores funcionales) F01 Usuario no existe
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  get = (request: express.Request, response: express.Response) => {
    const { expands } = request.query;
    UserService.get(getNumberParam(request, 'userId'), expands as string)
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }

/**
 * @api {get} /users?query=:query&sort=:sort&page=:page&pageSize=:pageSize 04 - Listar Usuario
 * @apiVersion 1.0.0
 * @apiName GetAll
 * @apiGroup User
 * @apiPermission authenticate
 *
 * @apiDescription Listar usuarios
 *
 * @apiUse HeadersParams
 *
 * @apiParam {String} query Consulta a realizar, ejemplo query={userId:12}
 * @apiParam {String} sort Campos por los que se ordena el resultado, ejemplo sort=firstName:1
 * @apiParam {Number} page Página a mostrar
 * @apiParam {Number} pageSize Número de filas por página
 *
 * @apiSuccess {Object[]} data Datos del usuario
 * @apiUse UserResponse
 *
 * @apiError (400 Errores funcionales) V01 Lista con los campos inválidos
 * @apiError (204 Errores funcionales) S03 No se encontraron resultados
 * @apiError (500 Errores de servidor) Z99 Descripción en el mensaje
 */
  query = (request: express.Request, response: express.Response) => {
    UserService.query(queryTransform(request))
      .then(responseOkHelper(response))
      .catch(responseErrorHelper(response));
  }
}

export default UserController;
