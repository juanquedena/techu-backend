import BaseService from './BaseService';
import { ApiException } from '../exceptions/ApiException';
import { Transaction, Data, Query } from '../models/models';

let instance:TransactionServiceInstance = null;

class TransactionServiceInstance extends BaseService<Transaction> {

  constructor() {
    super('transactions');
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  async query(query: Query): Promise<Data<Transaction>> {
    return this.repository.invokeGet(
      query.q,
      { _id: 0 },
      query.s || { transaction_id: 1 },
      query.ps,
      query.p,
    ).then((response: any) => {
      return new Promise((resolve, reject) => {
        if (response.data.length <= 0) {
          reject(new ApiException(204, 'S03'));
        } else {
          resolve(this.transformArrayOut(response.data, response.metadata));
        }
      });
    });
  }

  async get(transactionId: number): Promise<Data<Transaction>> {
    return this.repository.invokeGet(
      { transaction_id: transactionId },
      { _id: 0 },
    ).then((response: any) => {
      return new Promise<Data<Transaction>>((resolve, reject) => {
        if (response.data.length === 1) {
          resolve(this.transformOut(response.data[0]));
        } else {
          reject(new ApiException(400, 'F04'));
        }
      });
    });
  }

  async save(transaction: Transaction): Promise<Data<Transaction>> {
    const data = this.transformIn(transaction);
    return this.repository.invokeAutoincrement()
      .then(async (nextValue: number) => {
        const newTransaction = { ...data, date: new Date().toISOString(), transaction_id: nextValue };
        return this.repository.invokePost('post', newTransaction)
          .then(() => {
            return this.transformOut(newTransaction);
          });
      });
  }

  async replace(transactionId: number, transaction: Transaction): Promise<Data<Transaction>> {
    const editTransaction = this.transformIn(transaction);
    return this.repository.invokePost('patch', editTransaction, { transaction_id: transactionId })
      .then((patchTransaction: any) => {
        delete patchTransaction._id;
        return this.transformOut(patchTransaction);
      });
  }
}

export default new TransactionServiceInstance();
