import BaseRepository from '../repositories/BaseRepository';
import { Data } from '../models/models';
import { FunctionUtil } from '../utils/FunctionUtil';

export default class BaseService<T> {

  protected repository: BaseRepository;

  constructor(collection: string) {
    this.repository = new BaseRepository(collection);
  }

  protected transformIn(document: T) {
    return FunctionUtil.transPropsCamelToSnake(document);
  }

  protected transformDocument(document: any): T {
    return FunctionUtil.transPropsSnakeToCamel(document);
  }

  protected transformArrayOut(document: any[], metadata: any): Data<T> {
    return {
      data: document.map((e: any) => this.transformDocument(e)),
    };
  }

  protected transformOut(document: any): Data<T> {
    return { data: this.transformDocument(document) };
  }
}
