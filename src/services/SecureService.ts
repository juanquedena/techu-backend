// import { Repository } from '../repositories/Repository';

let instance:SecureServiceInstance = null;

class SecureServiceInstance {

  constructor() {
    if (!instance) {
      instance = this;
      // this._repository = new Repository();
    }
    return instance;
  }

  sendDocument(request:any, authToken:string): any {
    return {
      authToken,
      request,
      response: {} };
    /*
    this._repository.invokeGet().then(result => {
      console.log(result.res);
      console.log(result.body);
    });
    */
  }
}

export default new SecureServiceInstance();
