import BaseService from './BaseService';
import { ApiException } from '../exceptions/ApiException';
import { Account, Data, Query } from '../models/models';
import e = require('express');

let instance:AccountServiceInstance = null;

class AccountServiceInstance extends BaseService<Account> {

  constructor() {
    super('users');
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  async queryAccount(iban: any): Promise<Data<Account>> {
    return this.repository.invokeGet(
      { 'accounts.iban': iban },
      {
        _id: 0,
        user_id: 1,
        first_name: 1,
        last_name: 1,
        accounts: {
          $filter: {
            input: '$accounts',
            as: 'accounts',
            cond: { $eq: ['$$accounts.iban', iban] },
          },
        },
      },
    ).then((response: any) => {
      return new Promise((resolve, reject) => {
        if (response.data.length <= 0) {
          reject(new ApiException(400, 'F03'));
        } else {
          if (response.data[0].accounts !== undefined && response.data[0].accounts.length <= 0) {
            reject(new ApiException(204, 'S03'));
          } else {
            const data = response.data[0];
            const accounts: any = this.transformArrayOut(data.accounts, {});
            // accounts
            resolve({ data: {
              ...accounts.data[0],
              user: {
                userId: data.user_id,
                firstName: data.first_name,
                lastName: data.last_name,
              },
            } });
          }
        }
      });
    });
  }

  async query(userId: number, query: Query): Promise<Data<Account>> {
    return this.repository.invokeGet(
      { user_id: userId },
      {
        _id: 0,
        accounts: 1,
      },
      query.s || { 'accounts.account_id': -1 },
      query.ps,
      query.p,
    ).then((response: any) => {
      return new Promise((resolve, reject) => {
        if (response.data.length <= 0) {
          reject(new ApiException(400, 'F03'));
        } else {
          if (response.data[0].accounts !== undefined && response.data[0].accounts.length <= 0) {
            reject(new ApiException(204, 'S03'));
          } else {
            resolve(this.transformArrayOut(response.data[0].accounts, {}));
          }
        }
      });
    });
  }

  async get(userId: number, accountId: number): Promise<Data<Account>> {
    return this.repository.invokeGet(
      { user_id: userId },
      {
        _id: 0,
        accounts: {
          $filter: {
            input: '$accounts',
            as: 'accounts',
            cond: { $eq: ['$$accounts.account_id', accountId] },
          },
        },
      },
    ).then((response: any) => {
      return new Promise<Data<Account>>((resolve, reject) => {
        if (response.data.length === 1 &&
          response.data[0].accounts !== undefined &&
          response.data[0].accounts.length === 1) {
          resolve(this.transformOut(response.data[0].accounts[0]));
        } else {
          reject(new ApiException(400, 'F03'));
        }
      });
    });
  }

  async save(userId: number, account: Account): Promise<Data<Account>> {
    const data = this.transformIn(account);
    return this.repository.invokeAutoincrement('accounts')
      .then(async (nextValue: number) => {
        const newAccount = { accounts: { ...data, account_id: nextValue } };
        return this.repository.invokePost('post_array', newAccount, { user_id: userId })
          .then((response) => {
            return this.transformOut(
              response.accounts.filter((accountResponse: any) => accountResponse.account_id === nextValue)[0]);
          });
      });
  }

  async replace(userId: number, accountId: number, account: Account): Promise<Data<Account>> {
    const editAccountTemp = this.transformIn(account);
    const editAccount: any = {};
    for (const key in editAccountTemp) {
      if (editAccountTemp.hasOwnProperty(key)) {
        editAccount[`accounts.$.${key}`] = editAccountTemp[key];
      }
    }
    const oid = { user_id: userId, accounts: { $elemMatch: { account_id: accountId } } };
    return this.repository.invokePost('patch', editAccount, oid)
      .then((response: any) => {
        return this.transformOut(response.accounts.filter((accountResponse: any) => accountResponse.account_id === accountId)[0]);
      });
  }
}

export default new AccountServiceInstance();
