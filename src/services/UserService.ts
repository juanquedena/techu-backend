import BaseService from './BaseService';
import { ApiException } from '../exceptions/ApiException';
import AuthenticationHelper from '../helper/authenticationHelper';
import { User, Data, Query } from '../models/models';

let instance:UserServiceInstance = null;

class UserServiceInstance extends BaseService<User> {

  constructor() {
    super('users');
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  protected transformOut(user: any, generateToken: boolean = false): Data<User> {
    return {
      data: this.transformDocument(user),
      authorization: !generateToken ? undefined : {
        accessToken: AuthenticationHelper.generateAccessToken(user),
        refreshToken: AuthenticationHelper.generateRefreshToken(user),
      },
    };
  }

  async query(query: Query): Promise<Data<User>> {
    return this.repository.invokeGet(
      query.q,
      { _id: 0,
        user_id: 1,
        first_name: 1,
        last_name: 1,
        email: 1,
        status: 1,
      },
      query.s || { user_id: 1 },
      query.ps,
      query.p,
    ).then((response: any) => {
      return new Promise((resolve, reject) => {
        if (response.data.length <= 0) {
          reject(new ApiException(204, 'S03'));
        } else {
          resolve(this.transformArrayOut(response.data, response.metadata));
        }
      });
    });
  }

  async get(userId: number, expands: string): Promise<Data<User>> {
    return this.repository.invokeGet(
      { user_id: userId },
      { _id: 0,
        first_name: 1,
        last_name: 1,
        email: 1,
        accounts: expands === 'accounts' ? 1 : undefined,
      },
    ).then((response: any) => {
      return new Promise<Data<User>>((resolve, reject) => {
        if (response.data.length === 1) {
          resolve(this.transformOut(response.data[0]));
        } else {
          reject(new ApiException(400, 'F01'));
        }
      });
    });
  }

  async save(user: User): Promise<Data<User>> {
    const data = this.transformIn(user);
    return this.repository.invokeAutoincrement()
      .then(async (nextValue: number) => {
        const newUser = { ...data, user_id: nextValue };
        return this.repository.invokePost('post', newUser)
          .then(() => {
            return this.transformOut(newUser);
          });
      });
  }

  async replace(userId: number, user: User): Promise<Data<User>> {
    const editUser = this.transformIn(user);
    return this.repository.invokePost('patch', editUser, { user_id: userId })
      .then((patchUser: any) => {
        delete patchUser._id;
        return this.transformOut(patchUser);
      });
  }

  async authenticate(username: string, password: string): Promise<Data<User>> {
    return this.repository.invokeGet(
      { email: username },
      { _id: 0,
        user_id: 1,
        first_name: 1,
        last_name: 1,
        password: 1 },
    ).then((response: any) => {
      return new Promise((resolve, reject) => {
        if (response.data.length < 1) {
          reject(new ApiException(400, 'F01'));
        } else {
          const user = response.data[0];
          if (user.password === password) {
            delete user.password;
            resolve(this.transformOut(user, true));
          } else {
            reject(new ApiException(400, 'F02'));
          }
        }
      });
    });
  }
}

export default new UserServiceInstance();
