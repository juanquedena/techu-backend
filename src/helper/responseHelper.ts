import * as express from 'express';
import { ApiException } from '../exceptions/ApiException';

export function responseErrorHelper(response: express.Response) {
  const t1 = new Date().getTime();
  return (err: any) => {
    const t3 = new Date().getTime();
    response.setHeader('time-execution', t3 - t1);
    let error = {
      status: 500,
      code: 'Z98',
      message: err.message,
      detail: err,
    };
    if (err instanceof ApiException) {
      error = err.getError();
    } else if (err.response !== undefined) {
      switch (err.response.status) {
        case 400:
        case 401:
        case 403:
        case 404:
          error.status = err.response.status;
          error.message = err.response.data.error;
          error.detail = err.response.data;
          break;
        case 409:
          error.status = err.response.status;
          error.message = 'Método no soportado';
          error.detail = 'Solo se soportan las operaciones GET|POST|PUT|PATCH|DELETE';
          break;
        default:
          error.code = 'Z97';
          error.message = err.message;
          error.detail = err;
          break;
      }
    }
    response.status(error.status).json(error);
  };
}

export function responseOkHelper(response: express.Response, callback?: (data: any) => object) {
  const t1 = new Date().getTime();
  return (data: any) => {
    const t2 = new Date().getTime();
    response.setHeader('time-execution', t2 - t1);
    if (callback !== undefined) {
      response.json(callback(data));
    } else {
      response.json(data);
    }
  };
}
