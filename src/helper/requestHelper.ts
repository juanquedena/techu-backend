import * as express from 'express';
import { Query } from '../models/models';
import { FunctionUtil } from '../utils/FunctionUtil';

export function queryTransform(request: express.Request): Query {
  const { query, fields, sort, page, pageSize } = request.query;
  return {
    q: query !== undefined ? FunctionUtil.transPropsCamelToSnake(JSON.parse(`${query}`.replace(/([_a-zA-Z0-9-]+):([ a-zA-Z0-9-]+)/g, '\"$1\":$2'))) : {},
    f: fields,
    s: sort !== undefined ? FunctionUtil.transPropsCamelToSnake(JSON.parse(`{${sort}}`.replace(/([_a-zA-Z0-9-]+):([ a-zA-Z0-9-]+)/g, '\"$1\":$2'))) : sort,
    p: parseInt(page as string, 10) || 1,
    ps: parseInt(pageSize as string, 10) || 10,
  };
}

export function getNumberParam(request: express.Request, key: string): number {
  return parseInt(request.params[key], 10);
}
