import * as jwt from 'jsonwebtoken';

const {
  SIGNATURE_ISSUER,
  SIGNATURE_SUBJECT,
  SIGNATURE_AUDIENCE,
  SIGNATURE_PRIVATE_KEY,
  SIGNATURE_PUBLIC_KEY,
  SIGNATURE_ALGORITHM } = process.env;

const signOptions: jwt.SignOptions = {
  issuer: SIGNATURE_ISSUER,
  subject: SIGNATURE_SUBJECT,
  audience: SIGNATURE_AUDIENCE,
  expiresIn: '1h',
  algorithm: SIGNATURE_ALGORITHM as jwt.Algorithm };

const TIMEOUT_ACCESS_TOKEN = '5m';
const TIMEOUT_REFRESH_TOKEN = '15m';
const TIMEOUT_SIGNATURE_TOKEN = '10s';

export default class AuthenticationHelper {

  private static generateToken(payload: any, key: string, expireIn: string) {
    signOptions.expiresIn = expireIn;
    return jwt.sign(payload, key, signOptions);
  }

  static generateAccessToken(payload: object) {
    return this.generateToken(payload, SIGNATURE_PRIVATE_KEY, TIMEOUT_ACCESS_TOKEN);
  }

  static generateRefreshToken(payload: object) {
    return this.generateToken(payload, SIGNATURE_PRIVATE_KEY, TIMEOUT_REFRESH_TOKEN);
  }

  static signaturePayload(payload: object, headers: object) {
    return this.generateToken(payload, SIGNATURE_PRIVATE_KEY, TIMEOUT_SIGNATURE_TOKEN);
  }

  static verifyAccessToken(token: string, callback?: jwt.VerifyCallback) {
    signOptions.expiresIn = '5m';
    return jwt.verify(token, SIGNATURE_PUBLIC_KEY, signOptions, callback);
  }

  static verifySignature(payload: string, headers: any, callback?: jwt.VerifyCallback) {
    const options: any = {
      issuer: headers['signature-issuer'],
      subject: headers['signature-subject'],
      audience: SIGNATURE_AUDIENCE,
      expiresIn: TIMEOUT_SIGNATURE_TOKEN,
      algorithm: SIGNATURE_ALGORITHM as jwt.Algorithm };
    return jwt.verify(payload, SIGNATURE_PUBLIC_KEY, options, callback);
  }
}
