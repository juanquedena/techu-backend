import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import BaseController from './controllers/BaseControllerInterface';
import errorMiddleware from './middlewares/errorMiddleware';
import { Server } from 'http';

type InitFunction = (app: express.Application) => void;

class App {
  public app: express.Application;
  public server: Server;
  public port: number;
  public path: string;

  constructor(
      path: string,
      port: string = '3000',
      init: InitFunction,
      controllers: BaseController[] = [],
      middlewares: express.RequestHandler[] = []) {
    this.app = express();
    this.port = parseInt(port, 10);
    this.path = path;
    init(this.app);
    this.initializeMiddlewares(middlewares);
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  private initializeMiddlewares(middlewares: express.RequestHandler[]) {
    /*
    var whitelist = ['http://example1.com', 'http://example2.com']
    var corsOptionsDelegate = function (req, callback) {
      var corsOptions;
      if (whitelist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
      } else {
        corsOptions = { origin: false } // disable CORS for this request
      }
      callback(null, corsOptions) // callback expects two parameters: error and options
    }
     */
    this.app.use(cors());
    this.app.use(bodyParser.json());
    middlewares.forEach((middleware: express.RequestHandler) => {
      this.app.use(middleware);
    });
  }

  private initializeControllers(controllers: BaseController[]) {
    controllers.forEach((controller: BaseController) => {
      this.app.use(this.path, controller.router);
    });
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  public start() {
    this.server = this.app.listen(this.port, '0.0.0.0', () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }

  public getApplicationInstance(): express.Application {
    return this.app;
  }

  public getServer() {
    return this.server;
  }
}

export default App;
