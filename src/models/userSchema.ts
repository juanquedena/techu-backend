import BaseSchema from './baseSchema';
import { ValidationChain, param, body, query } from 'express-validator';

const userId: ValidationChain[] = [
  param('userId', 'Identificador de registro inválido')
    .isInt({ gt: 0 }),
];

const user: ValidationChain[] = [
  body('firstName')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese los nombres')
    .bail()
    .custom(BaseSchema.isLetter)
    .withMessage('Los nombres solo debe contener letras'),
  body('lastName')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese los apellidos')
    .bail()
    .custom(BaseSchema.isLetter)
    .withMessage('Los apellidos solo debe contener letras'),
  body('email')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese su correo electrónico')
    .bail()
    .if(BaseSchema.notIsPatch)
    .isEmail()
    .withMessage('Dirección de correo electrónico inválido'),
];

const userIdSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...userId,
  query('expands')
    .optional({ nullable: true })
    .isIn(['accounts'])
    .withMessage('Valor del atributo no soportado'),
];

const userPostSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...user,
];

const userPatchSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...userId,
  ...user,
];

export default {
  userId,
  userIdSchema,
  userPostSchema,
  userPatchSchema,
};
