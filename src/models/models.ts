export interface Header {
  applicationCode: string;
  channelCode: string;
}

export interface Payload {
  collection: string;
  operation: string;
  item: any;
  oid?: string;
}

export interface Message {
  header: Header;
  payload: Payload;
}

export interface Card {
  number?: string;
  type?: string;
}

export interface Account {
  accountId?: number;
  iban?: string;
  currencyCode?: string;
  balance?: number;
  card?: Card;
  status?: string;
}

export interface User {
  userId?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  accounts?: Account[];
  status?: string;
}

export interface Transaction {
  transaction_id?: number;
  description?: string;
  date?: string;
  type?: string;
  amount?: number;
  userId?: number;
  accountId?: number;
}

export interface Authorization {
  accessToken: string;
  refreshToken: string;
}

export interface Data<T> {
  data: T | T[];
  authorization?: Authorization;
}

export interface Query {
  q?: any;
  f?: any;
  s?: any;
  p: number;
  ps: number ;
}
