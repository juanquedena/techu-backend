import BaseSchema from './baseSchema';
import { ValidationChain, body } from 'express-validator';

const secureSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  body('operation')
    .isIn(['get', 'delete', 'post', 'put', 'patch'])
    .withMessage('El operación no esta soportada'),
  body('collection', 'Nombre de coleción inválido')
    .isString(),
  body('uuid', 'Identificador de registro inválido')
    .custom((value, { req, location, path }) => {
      const regex = /(delete|put|path)/gm;
      if (regex.test(req.body.operation)) {
        return value !== undefined;
      }
      return true;
    }),
  body('document', 'El documento es obligatorio')
    .custom((value, { req, location, path }) => {
      const regex = /(post|put|path)/gm;
      if (regex.test(req.body.operation)) {
        return value !== undefined;
      }
      return true;
    }),
];

export default { secureSchema };
