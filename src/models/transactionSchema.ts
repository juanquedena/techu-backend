import BaseSchema from './baseSchema';
import { ValidationChain, param, body } from 'express-validator';

const transactionId: ValidationChain[] = [
  param('transactionId', 'Identificador de registro inválido')
    .isInt({ gt: 0 }),
];

const transaction: ValidationChain[] = [
  body('userId', 'Identificador de usuario inválido')
    .if(BaseSchema.notIsPatch)
    .isInt({ gt: 0 }),
  body('accountId', 'Identificador de cuenta inválido')
    .if(BaseSchema.notIsPatch)
    .isInt({ gt: 0 }),
  body('description')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese la descripción'),
  body('amount')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese el importe del movimiento')
    .bail()
    .isNumeric(),
  body('type', 'Tipo de movimiento inválido')
    .if(BaseSchema.notIsPatch)
    .isIn(['TO HAVE', 'SHOULD']), // 'ENTRY', 'EGRESS'
];

const transactionIdSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...transactionId,
];

const transactionPostSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...transaction,
];

const transactionPatchSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...transactionId,
  ...transaction,
];

export default {
  transactionId,
  transactionIdSchema,
  transactionPostSchema,
  transactionPatchSchema,
};
