import BaseSchema from './baseSchema';
import { ValidationChain, param, body, query } from 'express-validator';

const accountId: ValidationChain[] = [
  param('accountId', 'Identificador de registro inválido')
    .isInt({ gt: 0 }),
];

const account: ValidationChain[] = [
  body('iban')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese el código IBAN')
    .bail()
    .if(BaseSchema.notIsPatch)
    .isIBAN()
    .withMessage('Código IBAN inválido'),
  body('currencyCode')
    .custom(BaseSchema.optionalForPatch)
    .withMessage('Ingrese el código de la moneda')
    .bail()
    .if(BaseSchema.notIsPatch)
    .isIn(['PEN', 'EUR', 'USD'])
    .withMessage('Código de moneda no soportado'),
  body('balance', 'Saldo inválido')
    .optional({ nullable: true })
    .isDecimal(),
  body('card.number', 'Número de tarjeta inválido')
    .if(BaseSchema.notIsPatch)
    .isCreditCard(),
  body('card.type', 'Tipo de tarjeta inválido')
    .if(BaseSchema.notIsPatch)
    .isIn(['Visa', 'Master Card']),
];

const accountIdSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...accountId,
  query('expands')
    .optional({ nullable: true })
    .isIn(['transactions'])
    .withMessage('Valor del atributo no soportado'),
];

const accountPostSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...account,
];

const accountPatchSchema: ValidationChain[] = [
  ...BaseSchema.headerSchema,
  ...accountId,
  ...account,
];

export default {
  accountId,
  accountIdSchema,
  accountPostSchema,
  accountPatchSchema,
};
