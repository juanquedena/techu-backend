import { CustomValidator, ValidationChain, header, query, body } from 'express-validator';

const isPassword: CustomValidator = (value, { req, location, path }) => {
  const regex = /^((?=.*[a-z])(?=.*[A-Z]).{6,32})$/gm;
  return value !== undefined && regex.test(value);
};

const isLetter: CustomValidator = (value, { req, location, path }) => {
  const regex = /[ ñáéíóúa-z ÑÁÉÍÓÚA-Z\s]*/gm;
  return value === undefined || regex.test(value);
};

const optionalForPatch: CustomValidator = (value, { req, location, path }) => {
  return req.method.toLowerCase() === 'patch' || value !== undefined;
};

const notIsPatch: CustomValidator = (value, { req }) => req.method.toLowerCase() !== 'patch';

const headerSchema: ValidationChain[] = [
  header('signature-issuer')
    .custom((value, { req, location, path }) => {
      const tokenRefresh = req.headers['token-refresh'];
      const signatureSubject = req.headers['signature-subject'];
      return (
        value !== undefined &&
        signatureSubject !== undefined &&
        (tokenRefresh !== undefined || req.path === '/secure/signIn')
      );
    })
    .withMessage('Cabeceras de autentificación inválidos'),
  body('status')
    .optional({ nullable: true })
    .isIn(['A', 'I'])
    .withMessage('Valor invalido, solo se aceptan los valores: [A, I]'),
];

const querySchema: ValidationChain[] = [
  query('query').optional({ nullable: true }),
  query('sort').optional({ nullable: true }),
  query('fields').optional({ nullable: true }),
  query('pageSize', 'Valor inválido')
    .optional({ nullable: true })
    .isInt(),
  query('page', 'Valor inválido')
    .optional({ nullable: true })
    .isInt(),
];

export default {
  headerSchema,
  querySchema,
  isPassword,
  isLetter,
  optionalForPatch,
  notIsPatch,
};
