import BaseSchema from './baseSchema';
import { body } from 'express-validator';

export default [
  ...BaseSchema.headerSchema,
  body('username')
    .isEmail()
    .withMessage('Ingrese un nombre de usuario válido'),
  body('password')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('Ingrese su contraseña')
    .bail()
    .custom(BaseSchema.isPassword)
    .withMessage('Contraseña inválida'),
];
