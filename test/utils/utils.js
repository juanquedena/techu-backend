const addContext = require('mochawesome/addContext');

"use strict";
exports.__esModule = true;
exports.utils = {
  addContextUtil(context, value) {
    addContext(context, value);
  }
};