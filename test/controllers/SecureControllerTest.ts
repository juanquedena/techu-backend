import {
  checkResponseError,
  checkStatus,
  checkMessagesError,
  checkResponse,
  checkData,
  authenticate,
  postWithAuthenticate,
  postWithoutAuthenticate } from './BaseControllerTest';
import { utils } from '../utils/utils';

export class SecureControllerTest {

  static async createRequestSignIn(request: any) {
    return authenticate(request);
  }

  static async createRequestSend(request: any, useToken: boolean = true) {
    if (useToken) {
      return postWithAuthenticate('secure', request, {
        username: 'isemmence0@google.nl',
        password: 'VxlKwL',
      });
    }

    return postWithoutAuthenticate('secure', request);
  }
}

let response: any;

describe('Secure Controller Test', function () {

  this.timeout(5000);

  describe('Sign in: authenticate valid', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSignIn({
        username: 'isemmence0@google.nl',
        password: 'VxlKwL',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 200', () => checkStatus(response, 200));
    it('Check structure', () => {
      checkResponse(response, [{
        key: 'data', type: 'object',
        fields: [
          { key: 'userId', type: 'number' },
          { key: 'firstName', type: 'string' },
          { key: 'lastName', type: 'string' },
        ],
      }, {
        key: 'authorization', type: 'object',
        fields: [
          { key: 'accessToken', type: 'string' },
          { key: 'refreshToken', type: 'string' },
        ],
      }]);
    });
    it('Check data', () => {
      checkData(response.body, {
        'data.userId': 1,
        'data.firstName': 'Irma',
        'data.lastName': 'Semmence',
      });
    });
  });

  describe('Sign in: password invalid', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSignIn({
        username: 'isemmence0@google.nl',
        password: 'VxlKwL1',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'F02'));
  });

  describe('Sign in: name invalid', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSignIn({
        username: 'isemmence0@googleeee.nl',
        password: 'VxlKwL1',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'F01'));
  });

  describe('Sign in: without username', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSignIn({
        password: 'VxlKwL',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'V01'));
    it('Check messages error', () => checkMessagesError(response, [{
      index: 0,
      param: 'username',
      location: 'body',
      msg: 'Ingrese un nombre de usuario válido',
    }]));
  });

  describe('Sign in: without password', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSignIn({
        username: 'isemmence0@google.nl',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'V01'));
    it('Check messages error', () => checkMessagesError(response, [{
      index: 0,
      param: 'password',
      location: 'body',
      msg: 'Ingrese su contraseña',
    }]));
  });

  describe('Send: without authorization', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSend({}, false);
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 401', () => checkStatus(response, 401));
    it('Check structure', () => checkResponseError(response, 401, 'S01'));
  });

  describe('Send: without parameters', () => {

    before(async function () {
      response = await SecureControllerTest.createRequestSend({});
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'V01'));
  });

});
