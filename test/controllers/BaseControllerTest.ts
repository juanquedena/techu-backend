import '../MainTest';
import { agent as requestTest } from 'supertest';
import { expect } from 'chai';
import { appSecure } from '../../src/Server';
import { CodeError } from '../../src/exceptions/ApiException';

const { API_URL } = process.env;

export const signatureSubject = 'signature-subject';
export const signatureIssuer = 'signature-issuer';

export async function authenticate(user: any) {
  return requestTest(appSecure.getApplicationInstance())
    .post(`${API_URL}/secure/signIn`)
    .set('signature-subject', signatureSubject)
    .set('signature-issuer', signatureIssuer)
    .send(user);
}

export async function postWithoutAuthenticate(path: string, request: any) {
  return requestTest(appSecure.getApplicationInstance())
    .post(`${API_URL}/${path}`)
    .send(request);
}

export async function postWithAuthenticate(path: string, request: any, user: any) {
  const response = await authenticate(user);
  return requestTest(appSecure.getApplicationInstance())
    .post(`${API_URL}/${path}`)
    .send(request)
    .set('signature-subject', signatureSubject)
    .set('signature-issuer', signatureIssuer)
    .set('token-refresh', response.body.authorization.refreshToken)
    .auth(response.body.authorization.accessToken, { type: 'bearer' });
}

export async function patchWithAuthenticate(path: string, request: any, user: any) {
  const response = await authenticate(user);
  return requestTest(appSecure.getApplicationInstance())
    .patch(`${API_URL}/${path}`)
    .send(request)
    .set('signature-subject', signatureSubject)
    .set('signature-issuer', signatureIssuer)
    .set('token-refresh', response.body.authorization.refreshToken)
    .auth(response.body.authorization.accessToken, { type: 'bearer' });
}

export async function getWithAuthenticate(path: string, user: any) {
  const response = await authenticate(user);
  return requestTest(appSecure.getApplicationInstance())
    .get(`${API_URL}/${path}`)
    .set('signature-subject', signatureSubject)
    .set('signature-issuer', signatureIssuer)
    .set('token-refresh', response.body.authorization.refreshToken)
    .auth(response.body.authorization.accessToken, { type: 'bearer' });
}

export type CheckType = 'string' | 'number' | 'object' | 'array';

export interface CheckField {
  key: string;
  type: CheckType;
  fields?: CheckField[];
}

export function checkStatus(response: any, status:number) {
  expect(response.status).to.equal(status);
}

export function checkResponse(response: any, checkFields: CheckField[]) {
  expect(response.body).to.be.an('object', 'Not exists body');

  const check = (data: any, fields?: CheckField[]) => {
    for (const row of fields) {
      const item = data[row.key];
      expect(item).to.be.an(row.type, `Not exists $.${row.key} or diferent type ${row.type}`);
      if (row.type === 'object') {
        for (const field of row.fields) {
          check(item, [field]);
        }
      } else if (row.type === 'array') {
        for (const rowItem of item) {
          check(rowItem, row.fields);
        }
      }
    }
  };

  check(response.body, checkFields);
}

export function checkData(data: any, fields: any) {
  expect(data).to.have.nested.includes(fields);
}

export function checkResponseError(response: any, status: number, code: CodeError, messages?: any[]) {
  expect(response.body).to.be.an('object', 'Not exists body');
  expect(response.body.status).to.be.an('number').equal(status);
  expect(response.body.code).to.be.an('string').equal(code);
  if (code === 'V01') {
    expect(response.body.messages).to.be.an('array');
    expect(response.body.messages.length).to.be.gte(1);
  } else {
    expect(response.body.message).to.be.an('string');
  }
}

export function checkMessagesError(response: any, messages: any[]) {
  for (const message of messages) {
    const { index, param, location, msg } = message;
    expect(response.body.messages[index]).to.be.an('object');
    expect(response.body.messages[index].param).to.be.an('string').equal(param);
    expect(response.body.messages[index].location).to.be.an('string').equal(location);
    expect(response.body.messages[index].msg).to.be.an('string').equal(msg);
  }
}
