import {
  checkResponseError,
  checkStatus,
  checkResponse,
  checkData,
  getWithAuthenticate,
  postWithAuthenticate,
  patchWithAuthenticate } from './BaseControllerTest';
import { utils } from '../utils/utils';
import { User } from '../../src/models/models';

export class UserControllerTest {

  static async createRequestGetById(userId: any) {
    return getWithAuthenticate(`users/${userId}`, {
      username: 'isemmence0@google.nl',
      password: 'VxlKwL',
    });
  }

  static async createRequestPost(data: any) {
    return postWithAuthenticate('users', data, {
      username: 'isemmence0@google.nl',
      password: 'VxlKwL',
    });
  }

  static async createRequestPatch(userId: string, data: any) {
    return patchWithAuthenticate(`users/${userId}`, data, {
      username: 'isemmence0@google.nl',
      password: 'VxlKwL',
    });
  }
}

let response: any;
let user: User;

describe('User Controller Test', function () {

  this.timeout(5000);

  describe('Post create user: Ok', () => {

    before(async function () {
      response = await UserControllerTest.createRequestPost({
        firstName: 'user name',
        lastName: 'last name',
        email: 'email@gmail.nl',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
      user = response.body.data;
    });

    it('Check status: 200', () => checkStatus(response, 200));
    it('Check structure', () => {
      checkResponse(response, [{
        key: 'data', type: 'object',
        fields: [
          { key: 'userId', type: 'number' },
          { key: 'firstName', type: 'string' },
          { key: 'lastName', type: 'string' },
          { key: 'email', type: 'string' },
        ],
      }]);
    });
  });

  describe('Post create user: Invalid', () => {

    before(async function () {
      response = await UserControllerTest.createRequestPost({
        firstName: 'user name 1',
        email: 'email@a.nl',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => checkResponseError(response, 400, 'V01'));
  });

  describe('Patch modify user: Ok', () => {

    before(async function () {
      response = await UserControllerTest.createRequestPatch(`${user.userId}`, {
        firstName: 'patch name',
      });
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 200', () => checkStatus(response, 200));
    it('Check structure', () => {
      checkResponse(response, [{
        key: 'data', type: 'object',
        fields: [
          { key: 'firstName', type: 'string' },
          { key: 'lastName', type: 'string' },
          { key: 'email', type: 'string' },
        ],
      }]);
    });
    it('Check data', () => {
      checkData(response.body, {
        'data.firstName': 'patch name',
        'data.lastName': user.lastName,
        'data.email': user.email,
      });
    });
  });

  describe('Get by userId: Ok', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById(10);
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 200', () => checkStatus(response, 200));
    it('Check structure', () => {
      checkResponse(response, [{
        key: 'data', type: 'object',
        fields: [
          { key: 'firstName', type: 'string' },
          { key: 'lastName', type: 'string' },
          { key: 'email', type: 'string' },
        ],
      }]);
    });
  });

  describe('Get by userId with expands: Ok', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById('10?expands=accounts');
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 200', () => checkStatus(response, 200));
    it('Check structure', () => {
      checkResponse(response, [{
        key: 'data', type: 'object',
        fields: [
          { key: 'firstName', type: 'string' },
          { key: 'lastName', type: 'string' },
          { key: 'email', type: 'string' },
          { key: 'accounts', type: 'array',
            fields: [
              { key: 'accountId', type: 'number' },
              { key: 'iban', type: 'string' },
              { key: 'balance', type: 'number' },
              { key: 'card', type: 'object',
                fields: [
                  { key: 'number', type: 'string' },
                  { key: 'type', type: 'string' },
                ],
              },
            ],
          },
        ],
      }]);
    });
  });

  describe('Get by userId with expands: Invalid', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById('10?expands=accountsqqqq');
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => {
      checkResponseError(response, 400, 'V01');
    });
  });

  describe('Get by userId: Not found', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById(100);
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => {
      checkResponseError(response, 400, 'F01');
    });
  });

  describe('Get by userId: userId invalid number', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById(-10);
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => {
      checkResponseError(response, 400, 'V01');
    });
  });

  describe('Get by userId: userId letter', () => {

    before(async function () {
      response = await UserControllerTest.createRequestGetById('abc');
      utils.addContextUtil(this, { title: 'Response', value: response });
    });

    it('Check status: 400', () => checkStatus(response, 400));
    it('Check structure', () => {
      checkResponseError(response, 400, 'V01');
    });
  });
});
