FROM node:10.20.1-alpine3.11

WORKDIR /usr/src/app

COPY build ./build
COPY .env ./
COPY package*.json ./

RUN npm ci --only=production

CMD ["npm", "start"]